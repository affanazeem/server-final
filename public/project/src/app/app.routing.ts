import  { Routes, RouterModule} from '@angular/router';
import  {Layout2Component} from "./layout2/layout2.component";
import {DashboardComponent} from "./layout2/login/dashboard/dashboard.component";
import {Layout1Component} from "./layout1/layout1.component";
import {LoginComponent} from "./layout2/login/login.component";
import {RegisterComponent} from "./layout2/register/register.component";
import {AdminComponent} from "./layout2/register/admin/admin.component";
import {InstructdbComponent} from "./layout2/login/contentarea/instructdb/instructdb.component";
import {StaffdbComponent} from "./layout2/login/contentarea/staffdb/staffdb.component";
import {AdmindbComponent} from "./layout2/login/contentarea/admindb/admindb.component";
import {ViewteacherComponent} from "./layout2/login/contentarea/admindb/adminca/viewteacher/viewteacher.component";
import {ViewstdComponent} from "./layout2/login/contentarea/admindb/adminca/viewstd/viewstd.component";
import {AdmincaComponent} from "./layout2/login/contentarea/admindb/adminca/adminca.component";
import {InstructorComponent} from "./layout2/login/contentarea/admindb/adminca/instructor/instructor.component";
import {InstructcaComponent} from "./layout2/login/contentarea/instructdb/instructca/instructca.component";
import {PublishresultComponent} from "./layout2/login/contentarea/instructdb/instructca/publishresult/publishresult.component";
import {ManageexamsComponent} from "./layout2/login/contentarea/instructdb/instructca/manageexams/manageexams.component";
import {SectionsComponent} from "./layout2/login/contentarea/instructdb/instructca/sections/sections.component";
import {HttptestComponent} from "./layout1/httptest/httptest.component";

  const APP_ROUTES: Routes = [


  {path : 'layout1' , component: Layout1Component},


  {path : '', redirectTo : '/layout1', pathMatch : 'full'},

    {path: 'instructdb' , component: InstructdbComponent, children:[

      {path: 'instructca' , component: InstructcaComponent, children:[

        {path: 'publishresult' , component: PublishresultComponent},

        {path: 'manageexams' , component: ManageexamsComponent},

        {path: 'sections' , component: SectionsComponent}

      ]}
      ]},


    // {path: 'layout2' , component: Layout2Component},
  //
  // {path: 'login' , component: LoginComponent},
  //
  // {path: 'register' , component: RegisterComponent},
  //b' , component: AdmindbComponent, children: [

    {path: 'adminca' , component: AdmincaComponent, children:[

      {path: 'viewstd' , component: ViewstdComponent},

      {path: 'viewteacher' , component: ViewteacherComponent},

      {path: 'instructor' , component: InstructorComponent}
    ]},
  ];
  // {path: 'admin' , component: AdminComponent},
  //
  // {path: 'instructor' , component: InstructorComponent},
  //
  // {path: 'instructdb' , component: InstructdbComponent},
  //
  // {path: 'staffdb' , component: StaffdbComponent},

export const routing = RouterModule.forRoot(APP_ROUTES);
