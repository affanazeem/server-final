import { Component, OnInit } from '@angular/core';
import {TestingService} from "../testing.service";

@Component({
  selector: 'app-httptest',
  templateUrl: './httptest.component.html',
  styleUrls: ['./httptest.component.css']
})
export class HttptestComponent implements OnInit {

  getData: string;
  postData: string;

  constructor(private _httpService: TestingService) { }

  ngOnInit() {
  }


  onTestGet(){
    console.log('here!');
    this._httpService.getData().subscribe(
      data => this.getData = JSON.stringify(data),
      error => alert(error),
      () => console.log('yafoo!')

    )

  }
}
