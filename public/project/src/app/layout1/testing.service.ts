
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class TestingService {

  constructor(private _http: Http) { }

  getData(){
    return this._http.get(' http://http://localhost:3000/admins/name').map(res => res.json())

  }


}
