import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import * as $ from 'jquery';


@Component({
  selector: 'app-layout1',
  templateUrl: './layout1.component.html',
  styleUrls: ['./layout1.component.css']
})
export class Layout1Component implements OnInit {

  username: string;
  password: string;

  loginmatch: boolean = false;


  constructor(private router: Router) {
  }

  adminname: string;
  adminpassword: string;


  ngOnInit() {
  }


  InstructorReg() {
    console.log('isn');
    this.router.navigate(['/instructor'])
  }


  staffReg() {
    this.router.navigate(['/instructor'])
  }

  adminReg() {
    this.router.navigate(['/instructor'])
  }

  Instructor() {
    $(".call_modal_faculty").show(function () {
      $(".modal_faculty").fadeIn();
    });
    $(".closer_faculty").click(function () {
      $(".modal_faculty").fadeOut();
    });
  }


  facultylogin() {
    if (this.username == 'instruct' && this.password == 'abc') {
      console.log(this.username);
      this.router.navigate(['/instructdb']);
    }
    else {
      this.loginmatch = true;
    }
  }

  staff() {
    $(".call_modal_staff").show(function () {
      $(".modal_staff").fadeIn();
    });
    $(".closer_staff").click(function () {
      $(".modal_staff").fadeOut();
    });
  }

  stafflogin() {

    if (this.username == 'staff' && this.password == 'xyz') {
      console.log(this.username);

      this.router.navigate(['/staffdb']);
    }

    else {
      this.loginmatch = true;
    }
  }



  admin() {
    $(".call_modal").show(function () {
      $(".modal").fadeIn();

    });
    $(".closer").click(function () {
      $(".modal").fadeOut();
    });
  }


  adminlogin() {
    if (this.adminname == 'admin' && this.adminpassword == '123') {
      this.loginmatch = false;
      this.router.navigate(['/admindb'])
    }

    else {
      this.loginmatch = true;
    }

  }



}

