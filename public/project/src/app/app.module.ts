
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {routing} from "./app.routing";
import {RouterModule} from '@angular/router';
import {ServicesService} from './layout2/register/services.service';

import { AppComponent } from './app.component';
import { Layout1Component } from './layout1/layout1.component';
import { Layout2Component } from './layout2/layout2.component';
import { LoginComponent } from './layout2/login/login.component';
import { RegisterComponent } from './layout2/register/register.component';
import { DashboardComponent } from './layout2/login/dashboard/dashboard.component';
import {InstructorComponent} from "./layout2/login/contentarea/admindb/adminca/instructor/instructor.component";
import { AdminComponent } from './layout2/register/admin/admin.component';
import { HomeComponent } from './layout1/home/home.component';
import { ContentareaComponent } from './layout2/login/contentarea/contentarea.component';
import { StaffdbComponent } from './layout2/login/contentarea/staffdb/staffdb.component';
import { InstructdbComponent } from './layout2/login/contentarea/instructdb/instructdb.component';
import { FooterComponent } from './layout2/login/dashboard/footer/footer.component';
import { HeaderComponent } from './layout2/login/dashboard/header/header.component';
import { AdminlpComponent } from './layout2/login/dashboard/adminlp/adminlp.component';
import { AdmindbComponent } from './layout2/login/contentarea/admindb/admindb.component';
import { AdmincaComponent } from './layout2/login/contentarea/admindb/adminca/adminca.component';
import { ViewstdComponent } from './layout2/login/contentarea/admindb/adminca/viewstd/viewstd.component';
import { ViewteacherComponent } from './layout2/login/contentarea/admindb/adminca/viewteacher/viewteacher.component';
import { LeftpaneComponent } from './layout2/login/contentarea/admindb/leftpane/leftpane.component';
import { InstructcaComponent } from './layout2/login/contentarea/instructdb/instructca/instructca.component';
import { InstructLeftpaneComponent } from './layout2/login/contentarea/instructdb/instruct-leftpane/instruct-leftpane.component';
import { PublishresultComponent } from './layout2/login/contentarea/instructdb/instructca/publishresult/publishresult.component';
import { ManageexamsComponent } from './layout2/login/contentarea/instructdb/instructca/manageexams/manageexams.component';
import { SectionsComponent } from './layout2/login/contentarea/instructdb/instructca/sections/sections.component';
import { HttptestComponent } from './layout1/httptest/httptest.component';
import {TestingService} from "./layout1/testing.service";
import { ViewstaffComponent } from './layout2/login/contentarea/admindb/adminca/viewstaff/viewstaff.component';


@NgModule({
  declarations: [
    AppComponent,
    Layout1Component,
    Layout2Component,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    InstructorComponent,
    AdminComponent,
    HomeComponent,
    StaffdbComponent,
    InstructdbComponent,
    ContentareaComponent,
    FooterComponent,
    HeaderComponent,
    AdminlpComponent,
    AdmindbComponent,
    AdmincaComponent,
    ViewstdComponent,
    ViewteacherComponent,
    LeftpaneComponent,
    InstructcaComponent,
    InstructLeftpaneComponent,
    PublishresultComponent,
    ManageexamsComponent,
    SectionsComponent,
    HttptestComponent,
    ViewstaffComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
  ],
  providers: [ServicesService, RouterModule, TestingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
