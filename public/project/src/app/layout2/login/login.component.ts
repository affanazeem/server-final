import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})
export class LoginComponent implements OnInit {

  username : string;
  password: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  login() {

    var name = /^[A-Z a-z _]+$/;

    if (this.username != "" && this.username.match(name)) {
      console.log(this.username);
      console.log(this.password);
      this.router.navigate(['/dashboard'])
    }
    else{
      alert('invalid username!');
    }
  }

  return(){
    this.router.navigate(['/layout1']);
  }
}
