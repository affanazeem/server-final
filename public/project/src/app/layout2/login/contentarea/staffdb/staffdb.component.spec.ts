import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffdbComponent } from './staffdb.component';

describe('StaffdbComponent', () => {
  let component: StaffdbComponent;
  let fixture: ComponentFixture<StaffdbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffdbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffdbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
