
import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import * as $ from 'jquery';
import {ServicesService} from "../../../../../register/services.service";


@Component({
  selector: 'app-instructor',
  templateUrl: './instructor.component.html',
  styleUrls: ['./instructor.component.css']
})
export class InstructorComponent implements OnInit {

  constructor(private router: Router, private dataService: ServicesService) { }

  disable : boolean = false;
  nameinvalid : boolean = true;
  namevalid: boolean = false;
  passlen: boolean = false;

  lnameinvalid : boolean = true;
  lnamevalid: boolean = false;


  interestinvalid : boolean = true;
  interestvalid: boolean = false;

  phoneinvalid : boolean = true;
  phonevalid : boolean = false;

  emailinvalid : boolean = true;
  emailvalid : boolean = false;

  passinvalid : boolean ;
  passvalid : boolean;

  phone_valid : any = /^[0-9]+$/;


  instructor : any = {id: 0, firstname: '', lastname: '', gender: '',  interest: '', phone : '', email: '', pass: ''}
  instruct_array = [];
  id: number = 0;

  fname : any = /^[A-Z a-z]+$/;
  phone : any = /^[0-9]+$/;
  ngOnInit() {
  this.nameinvalid = true;
  this.namevalid = false;

  this.lnameinvalid = true;
  this.lnamevalid = false;

  this.interestinvalid = true;
  this.interestvalid = false;


  this.phoneinvalid = true;
  this.phonevalid = false;


  this.emailinvalid = true;
  this.emailvalid = false;

  this.passinvalid;
  this.passvalid;

  }

  validatename() {
    if (this.instructor.firstname != null && this.instructor.firstname.match(this.fname)) {
      this.namevalid = true;
      this.nameinvalid = false;
    }
    else {
      this.namevalid = false;
      this.nameinvalid = true;
      this.disable = true;
    }
  }

  validatelname() {
    if (this.instructor.lastname != null && this.instructor.lastname.match(this.fname)) {
      this.lnamevalid = true;
      this.lnameinvalid = false;
    }
    else {
      this.lnamevalid = false;
      this.lnameinvalid = true;
      this.disable = true;
    }
  }

  validateinterest() {
    if (this.instructor.interest != null && this.instructor.interest.match(this.fname)) {
      this.interestvalid = true;
      this.interestinvalid = false;
    }
    else {
      this.interestvalid = false;
      this.interestinvalid = true;
      this.disable = true;
    }
  }


  validatephone(){
    if(this.instructor.phone!= null && this.instructor.phone.match(this.phone) && this.instructor.phone.length == 11){
      this.phonevalid = true;
      this.phoneinvalid = false;
    }
    else{
      this.phonevalid = false;
      this.phoneinvalid = true;
      this.disable = true;
    }
  }

    validateemail() {
      if (this.instructor.email != null && this.instructor.email.match(/^[A-z A-Z]*[0-9]*@[a-z A-Z]*[.][a-z A-Z]+$/)) {
        this.emailvalid = true;
        this.emailinvalid = false;
      }
      else {
        this.emailvalid = false;
        this.emailinvalid = true;
        this.disable = true;
      }
    }


  validatepassword() {

    if (this.instructor.pass != null && this.instructor.pass.length >=8 )  {
      this.passvalid = true;
      this.passinvalid = false;
      this.passlen = false;
    }
    else {
      this.passvalid = false;
      this.passinvalid = true;
      this.passlen = true;
      this.disable = true;

    }
  }

  validatelength(){
    if(this.instructor.pass.length <8){
      this.passinvalid = true;
      this.passlen = true;
    }
    else{
      this.passvalid = true;
      this.passlen = false;
      this.disable = true;
    }
  }



  create(){

    if(this.instructor.firstname!= null && this.instructor.firstname.match(this.fname) &&
      this.instructor.lastname!= null && this.instructor.lastname.match(this.fname)&&
      this.instructor.gender!= null && this.instructor.interest!= null && this.instructor.interest.match(this.fname) &&
      this.instructor.email!= null && this.instructor.phone != null && this.instructor.phone.length == 11 &&
      this.instructor.phone.match(this.phone_valid) && this.instructor.pass != null && this.instructor.pass.length >=7){


      this.disable = false;

      this.nameinvalid = false;
      this.phoneinvalid = false;
      this.emailinvalid = false;
      this.passinvalid = false;
      this.passvalid = true;
      this.passlen = false;

      this.id++;
      this.instruct_array.push(this.id);
      this.instruct_array.push(this.instructor.firstname);
      this.instruct_array.push(this.instructor.lastname);
      this.instruct_array.push(this.instructor.gender);
      this.instruct_array.push(this.instructor.interest);
      this.instruct_array.push(this.instructor.email);
      this.instruct_array.push(this.instructor.pass);


      $(".call_modal").show(function(){
        $(".modal").fadeIn();

      });

      $(document).ready(function(){
        $(".closer").click(function(){
          $(".modal").fadeOut();
        });
      });


      this.dataService.addData(this.instructor);

      // this.instructor.firstname = '';
      // this.instructor.lastname = '';
      // this.instructor.gender = '';
      // this.instructor.interest = '';
      // this.instructor.phone = '';
      // this.instructor.email = '';
      // this.instructor.pass = '';
      this.namevalid = false;
      this.lnamevalid = false;
      this.interestvalid = false;
      this.phonevalid = false;
      this.emailvalid = false;
      this.passvalid = false;


    }



    else if(this.instructor.pass.length <8){
    console.log(this.instructor.pass.length);
    this.passlen = false;
    this.passinvalid = true;
    this.passvalid = false;
    this.disable = true;
    }
  }


}
