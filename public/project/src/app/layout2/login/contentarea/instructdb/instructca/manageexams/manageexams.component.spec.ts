import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageexamsComponent } from './manageexams.component';

describe('ManageexamsComponent', () => {
  let component: ManageexamsComponent;
  let fixture: ComponentFixture<ManageexamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageexamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageexamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
