import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishresultComponent } from './publishresult.component';

describe('PublishresultComponent', () => {
  let component: PublishresultComponent;
  let fixture: ComponentFixture<PublishresultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishresultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
