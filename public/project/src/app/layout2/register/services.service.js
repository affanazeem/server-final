"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ServicesService = (function () {
    function ServicesService() {
        this.data = [];
        this.show = false;
    }
    ServicesService.prototype.addData = function (instructor) {
        this.show = true;
        this.data.push(instructor);
        console.log('name: ' + this.data[this.data.length - 1].firstname);
    };
    ServicesService.prototype.getData = function () {
        console.log("get chal raha hai ! " + this.data);
        return this.data;
    };
    ServicesService = __decorate([
        core_1.Injectable()
    ], ServicesService);
    return ServicesService;
}());
exports.ServicesService = ServicesService;
