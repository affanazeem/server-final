var express = require('express');
var router = express.Router();
var model = require("../models");
var constants = require("../config/constants");
var requestHelper = require("../helpers/request");
var responseHelper = require("../helpers/response");
var url = require('url');
var jwt = require('jsonwebtoken');
var multer = require('multer');



router.post('/loginteacher', function(req, res, next) {
    postBody = requestHelper.parseBody(req.body);

    model.teacher.find({where: {username:postBody.username, password:postBody.password}}).then(function (teachers) {

                if(teachers == null)
                {
                    res.statusCode = 404;
                    res.send('not registered!');
                    return res.json({loginteachermatch:true})
                }
                else {
                    res.statusCode = 200;
                    return res.json({loginteachermatch:false})
                 }})
            });



router.post('/sendNotification', function(req, res){

    postBody = requestHelper.parseBody(req.body);

    if(postBody.type=="invigilator"){

        model.notification.find({where:{to:postBody.to,from:postBody.from,classname:postBody.classname,subjectname:postBody.subjectname,
            type:postBody.type,expire:postBody.expire}}).then(function(notifications){

            if(notifications == null){
                model.notification.create({
                    to:postBody.to,
                    from:postBody.from,
                    classname:postBody.classname,
                    sectionname:postBody.sectionname,
                    subjectname:postBody.subjectname,
                    type:postBody.type,
                    expire:postBody.expire
                }).then(function(notifications){
                    res.statusCode = 200;
                    res.send(res.statusCode);
                })
            }
            else
            {
                res.statusCode = 400;
                res.send(res.statusCode);
            }
        })


    }

    else
    {
        model.notification.find({where:{to:postBody.to,from:postBody.from,classname:postBody.classname,sectionname:postBody.sectionname,subjectname:postBody.subjectname,
            type:postBody.type,expire:postBody.expire}}).then(function(notifications){

            if(notifications == null){
                model.notification.create({
                    to:postBody.to,
                    from:postBody.from,
                    classname:postBody.classname,
                    sectionname:postBody.sectionname,
                    subjectname:postBody.subjectname,
                    type:postBody.type,
                    expire:postBody.expire
                }).then(function(notifications){
                    res.statusCode = 200;
                    res.send(res.statusCode);
                })
            }
            else
            {
                res.statusCode = 400;
                res.send(res.statusCode);
            }
        })
    }

});


router.get('/NotificationOfTeacher/:firstname', function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;

    model.notification.findAll({where:{to:firstname,expire:0}}).then(function(notifications){
        if(notifications == null){
            res.statusCode = 400;
            res.send(res.statusCode);
        }

        else
        {
            res.statusCode = 200;
            res.send(notifications);
        }

    })
})


router.get('/returnNotification/:firstname/:lastname',function(req,res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;

    model.teacher.findAll({where:{firstname:firstname,lastname:lastname}},{
        include: model.datesheet
    }).then(function(teacher){
        model.datesheet.findAll().then(function(teachers){
            teacher.getDatesheets().then(function(datesheets){
                return res.json(datesheets);

            })
        })
    })
});


router.post('/addQuestion',function(req, res){

    postBody = requestHelper.parseBody(req.body);

    model.paper.find({where:{tname:postBody.tname,classname:postBody.classname,sectionname:postBody.sectionname
        ,subjectname:postBody.subjectname,content:postBody.content}}).then(function(papers){

            if(papers == null){

                    model.paper.create({tname:postBody.tname,
                    classname:postBody.classname,
                    sectionname:postBody.sectionname,
                    subjectname:postBody.subjectname,
                    content:postBody.content}).then(function(papers){

                        res.send('question added!');
                    })
                }

                else {
                res.statusCode = 400;
                res.send(res.statusCode);
            }
         })

});


router.get('/getAllQuestions/:tname/:classname/:sectionname/:subjectname',function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var tname = req.params.tname;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;
    var subjectname = req.params.subjectname;

    model.paper.findAll({where:{tname:tname, classname: classname, sectionname: sectionname, subjectname:subjectname}}).then(function(papers) {

        if (papers != null) {
            res.json(papers);
        }
        else {
            res.send('no questions added yet!');
        }
    })

})

router.delete('/DeleteQuestion/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    model.paper.destroy({where:{id:id}}).then(function (papers) {
        res.json(papers)
    })
});

router.get('/getQuestions/:id', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.paper.find({where:{id:id}}).then(function (papers) {
        res.json(papers)
    })
});



router.put('/UpdateQuestion/:id',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    postBody = requestHelper.parseBody(req.body);

    model.paper.find({where:{id:id}}).then(function (papers) {
        return papers.updateAttributes({tname:postBody.tname,classname:postBody.classname,
            sectionname:postBody.sectionname,subjectname:postBody.subjectname,content:postBody.content});

    }).then(function (papers) {
        res.json(papers);
    })

});


router.get('/getLoginTeacher/:post/:expire', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var post = req.params.post;
    var expire = req.params.expire;

    model.login.find({where:{post:"teacher",expire:true}}).then(function(logins){

        res.send(logins);
    })

})


router.get('/getname', function(req, res){

    model.login.find({where:{post:"teacher",expire:true}}).then(function(logins){
        res.send(logins);
    })

})
router.delete('/logoutteacher/:username/:password', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var username = req.params.username;
    var password = req.params.password;

    model.login.destroy({where:{username:username,password:password}}).then(function (logins) {
        res.json(logins)
    })
});


module.exports = router;

