var express = require('express');
var router = express.Router();
var model = require("../models");
var constants = require("../config/constants");
var requestHelper = require("../helpers/request");
var responseHelper = require("../helpers/response");
var url = require('url');
var jwt = require('jsonwebtoken');
var multer = require('multer');



router.post('/login', function(req, res){
    postBody = requestHelper.parseBody(req.body);
    var post = postBody.post;
    if(post == "admin"){
        model.admin.find({where:{username:postBody.username, password:postBody.password}}).then(function(admins){
            if(admins == null){
                res.statusCode = 400;
                res.send(res.statusCode);
            }
            else{
                model.admin.find({where:{username:postBody.username, password:postBody.password}}).then(function(logins) {
                    if(logins == null){
                    model.login.create({
                        username: postBody.username,
                        password: postBody.password,
                        post: "admin",
                        expire: true
                    })

                }
            })
        }})
    }

    else if(post == "teacher"){
        model.teacher.find({where:{username:postBody.username, password:postBody.password}}).then(function(teachers){
            if(teachers == null){
                res.statusCode = 400;
                res.send(res.statusCode);
            }
            else{
            model.login.find({where:{username:postBody.username,password:postBody.password}}).then(function(logins){
            if(logins == null) {
            model.login.create({
                username: postBody.username,
                password: postBody.password,
                post: "teacher",
                expire: true
            })
            }
          }
        )
    }
})
}
    else if(post == "student")
    {
        model.student.find({where:{username:postBody.username, password:postBody.password}}).then(function(students){
            if(students == null){
                res.statusCode = 400;
                res.send(res.statusCode);
            }
            else{
                model.login.find({where:{username:postBody.username, password:postBody.password}}).then(function(logins){

                if(logins == null) {
                    model.login.create({
                        username: postBody.username,
                        password: postBody.password,
                        post: "student",
                        expire: true
                    })
                }
            })
          }
        })
    }

    else if(post == "staff"){
        model.staff.find({where:{username:postBody.username, password:postBody.password}}).then(function(staffs){
            if(staffs == null){
                res.statusCode = 400;
                res.send(res.statusCode);
            }
            else{
                model.login.find({where:{username:postBody.username, password:postBody.password}}).then(function(logins){

                    if(logins == null) {
                           model.login.create({
                            username: postBody.username,
                            password: postBody.password,
                            post: "staff",
                            expire: true
                        })

                    }
                })
        }})
    }

    else {
        res.statusCode = 400;
        res.send(res.statusCode);
    }
});



router.get('/loginUser/:username/:password', function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var username = req.params.username;
    var password = req.params.password;


    model.login.find({where:{username:username,password:password,expire:true}}).then(function(logins){
        if (logins != null) {
        res.send(logins);
        }
        else
        {
            res.send('error')
        }
    })
})


router.get('/coursesOfclasses',function(req,res){

    model.course.find({where: {classname:"oop"}},{
        include: model.classes
    }).then(function(Class){
        Class.getSection().then(function(classes){
            res.json(classes);
        });
    });
});


var storage = multer.diskStorage({
    destination: function(req,file, cb){
        cb(null, 'uploads/')
    },
    filename: function(req, file, cb){
        cb(null, file.fieldname + '-'+ Date.now() + '.jpg')
    }
});

var upload  = multer({storage: storage}).single('profileImage');


router.post('/profile', function(req, res){

    upload(req, res, function(err){
        if(err){
        }

        res.json({
            success:true,
            message:'image uploaded'
        })
    });
});

//
// router.get('/logoutAdmin',function (req,res) {
//     var a;
//     req.session.destroy(function (err) {
//         req.logout();
//         a = true;
//         res.send(a=true);
//     })
// });




router.post('/classSectionActivites/announcements', function(req, res){

    postBody = requestHelper.parseBody(req.body);

    var classname = postBody.classname;
    var secname = postBody.secname;
    var announcement = postBody.announcement;

})


router.post('/verifyfirstname', function(req, res){

    postBody = requestHelper.parseBody(req.body);

    model.teacher.find({where:{firstname:postBody.firstname}}).then(function(teachers){
        if(teachers!=null){
            res.statusCode = 200;
            return res.json({firstnameinvalid:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({firtsnameinvalid:true})
        }
    })
});

router.post('/getClassBySection', function(req, res){

    postBody = requestHelper.parseBody(req.body);

    model.classes.find({where:{classname:postBody.classname}}).then(function(classes){
        if(classes==null){
            res.statusCode = 400;
            return res.json({firstnameinvalid:true})
        }
        else
        {
            res.statusCode = 200;
            return res.json({firtsnamevalid:true})
        }
    })
});






router.post('/verifylastname', function(req, res){

    postBody = requestHelper.parseBody(req.body);

    model.teacher.find({where:{firstname:postBody.firstname, lastname:postBody.lastname}}).then(function(teachers){
        if(teachers!=null){
            res.statusCode = 200;
            return res.json({lastnameinvalid:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({lastsnameinvalid:true})
        }

    })
});




router.post('/teacher',function(req, res){


    postBody = requestHelper.parseBody(req.body);

    model.teacher.find({where:{firstname:postBody.firstname, lastname:postBody.lastname}}).then(function(Teacher){
        model.course.find({where:{coursename:postBody.coursename}}).then(function(Course){
            Teacher.addCourse(Course).then(function(Course){
                res.send("Teacher Assigned!")
            })
        })

    })
    });


router.post('/AssignCoursesToteacher',function(req, res){


    postBody = requestHelper.parseBody(req.body);

    // var url_parts = url.parse(req.url,true);
    // var query =  url_parts.query;
    var firstname = postBody.firstname;
    var coursename = postBody.coursename;


    model.teacher.find({where:{firstname:firstname}}).then(function(Teacher){
        model.course.find({where:{coursename:coursename}}).then(function(Course){
            Teacher.addCourse(Course).then(function(Course){
                res.send("Teacher Assigned!")
            })
        })

    })
});


router.delete('/DeleteStudentByStudentId', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var studentId = req.query.studentId;

    model.student.destroy({where: {id:studentId}}).then(function (students) {
        res.json(students)
    })
});

router.delete('/DeleteStudentByStudentName', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var username = req.query.username;

    model.student.destroy({where: {username:username}}).then(function (students) {
        res.json(students)
    })
});

router.put('/UpdateStudent',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var studentId = req.query.studentId;

    model.student.find({where: {id:studentId}}).then(function (students) {
        return students.updateAttributes({username:req.body.username, firstname:req.body.firstname,lastname:req.body.lastname,
            gender:req.body.gender, classId:req.body.classId,sectionname:req.body.sectionname,phone:req.body.phone,
            email:req.body.email, parentemail:req.body.parentemail,password:req.body.password});
    }).then(function (students) {
        res.json(students);
    })
});





router.delete('/DeleteTeacherByTeacherId', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var teacherId = req.query.teacherId;

    model.teacher.destroy({where: {id:teacherId}}).then(function (teachers) {
        res.json(teachers)
    })
});

router.delete('/DeleteTeacherByTeacherName', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var username = req.query.username;

    model.teacher.destroy({where: {username:username}}).then(function (teachers) {
        res.json(teachers)
    })
});

router.put('/UpdateTeacher',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var teacherId = req.query.teacherId;

    model.teacher.find({where: {id:teacherId}}).then(function (teachers) {
        return teachers.updateAttributes({username:req.body.username, firstname:req.body.firstname,lastname:req.body.lastname,
            gender:req.body.gender,phone:req.body.phone, email:req.body.email,password:req.body.password,isInvigilator:req.body.isInvigilator});
    }).then(function (teachers) {
        res.json(teachers);
    })
});





router.delete('/DeleteStaffByStaffId', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var staffId = req.query.staffId;

    model.staff.destroy({where: {id:staffId}}).then(function (staffs) {
        res.json(staffs)
    })
});

router.delete('/DeleteStaffByStaffName', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var username = req.query.username;

    model.staff.destroy({where: {username:username}}).then(function (staffs) {
        res.json(staffs)
    })
});

router.put('/UpdateStaff',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var staffId = req.query.staffId;

    model.staff.find({where: {id:staffId}}).then(function (staffs) {
        return staffs.updateAttributes({username:req.body.username, firstname:req.body.firstname,lastname:req.body.lastname,
            gender:req.body.gender,interest:req.body.interest,phone:req.body.phone,email:req.body.email,password:req.body.password,
            isAdmin:req.body.isAdmin});
    }).then(function (staffs) {
        res.json(staffs);
    })
});







// router.get('/AssignCoursesToClasses',function(req, res){
//
//     var url_parts = url.parse(req.url,true);
//     var query =  url_parts.query;
//     var coursename = req.query.coursename;
//     var classname = req.query.classname;
//
//
//     model.course.find({where:{coursename: coursename}}).then(function (Course) {
//         model.classes.find({where:{classname:classname}}).then(function(Class){
//             Course.addClasses(Class).then(function(Class){
//                 res.send('Course Assigned!')
//             })
//         })
//
//     })
//
//
// });
//

// router.get('/classes/course',function(req,res){
//
//     model.classes.find({id:1},{
//         include: model.course
//     }).then(function(Class){
//         Class.getCourses().then(function(courses){
//             res.json(courses);
//         });
//     });
// });
//
//
//

router.post('/myname', function(req, res){
    postBody = requestHelper.parseBody(req.body);
    myname = postBody.myname;
    return res.json({myname:postBody.myname});

})


//
// router.get('/name', function(req, res){
//     return  res.json({name:postBody.myname});
//
// })



//**************USER MANAGEMENT*****************//
router.post('/loginAdmin', function(req, res, next) {
    postBody = requestHelper.parseBody(req.body);
    model.admin.findOne({where: {username:postBody.username, password:postBody.password}}).then(function (admins) {
        // const user ={admins}
        // const token=jwt.sign({user}, 'my_secret_key');
        // res.json({token:token})
        if(admins == null){
            model.staff.find({where:{firstname:postBody.username,password:postBody.password,isAdmin:true}}).then(function(staffs,err){
                if(staffs == null)
                {
                    res.statusCode = 404;
                    res.send(res.statusCode);
                }
                else
                {
                    return res.json(staffs);
                }
            })
        }
        else {
            return res.json(admins);
        }})
});




router.post('/addStaff',function (req, res) {
    postBody = requestHelper.parseBody(req.body);
        model.staff.create({
            username : postBody.username,
            firstname: postBody.firstname,
            lastname: postBody.lastname,
            gender: postBody.gender,
            interest: postBody.interest,
            phone: postBody.phone,
            email: postBody.email,
            password: postBody.password,
            isAdmin : postBody.isAdmin
        })

    res.send("Staff Added Successfully!");

});


router.post('/addStudent',function (req, res) {
    postBody = requestHelper.parseBody(req.body);
        model.student.create({
            username: postBody.username,
            firstname: postBody.firstname,
            lastname: postBody.lastname,
            gender: postBody.gender,
            classId: postBody.classId,
            sectionname: postBody.sectionname,
            phone: postBody.phone,
            email: postBody.email,
            parentemail: postBody.parentemail,
            password : postBody.password,
            expire: postBody.expire
            }).then(function (students) {
        });

        res.send("Student Added Successfully!");
    });

//
// router.post('/loginAdmin', function(req, res, next) {
//
//     postBody = requestHelper.parseBody(req.body);
//
//     var username = postBody.username;
//     var password = postBody.username;
//     var secret = "abc";
//
//     model.admin.find({username:username}).then(function (admins) {
//
//         postBody.session = {
//             username: username,
//             password: password
//         };
//
//         var token = jwt.sign(postBody.session, secret, {expiresIn: 60 * 5});
//         res.json({result: true, token: token}, res.json(admins))
//     });
//     res.json({result: false});
//
// });


router.put('/AssignAdminPostId',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.staff.find({where:{id:id}}).then(function (staffs) {
        return staffs.updateAttributes({username: req.body.username,firstname:req.body.firstname, lastname:req.body.lastname, gender:req.body.gender,
            interest: req.body.interest, phone: req.body.phone,email:req.body.email, password: req.body.pass, isAdmin: req.body.isAdmin});
    }).then(function (staffs) {
        res.json(staffs);
    })
});



router.put('/AssignAdminPostName',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.query.firstname;

    model.staff.find({where: {firstname:firstname}}).then(function (staffs) {
        return staffs.updateAttributes({username: req.body.username, firstname:req.body.firstname, lastname:req.body.lastname, gender:req.body.gender,
            interest: req.body.interest, phone: req.body.phone,email:req.body.email, password: req.body.password, isAdmin: req.body.isAdmin});
    }).then(function (staffs) {
        res.json(staffs);
    })
});


router.get('/viewAllStaff', function(req, res){

    model.staff.findAll().then(function(staffs){
        return res.json(staffs)

    })
});

router.get('/viewAllStudent', function(req, res){

    model.student.findAll().then(function(students){
        return res.json(students)

    })
});

router.get('/viewAllTeacher', function(req, res){

    model.teacher.findAll().then(function(teachers){
        return res.json(teachers)

    })
});


router.delete('/DeleteStaff/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.staff.destroy({where: {id:id}}).then(function (staffs) {
        res.json(staffs)
    })
});


router.delete('/DeleteTeacher/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.teacher.destroy({where: {id:id}}).then(function (teachers) {
        res.json(teacher)
    })
});


router.delete('/DeleteStudent/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.student.destroy({where: {id:id}}).then(function (students) {
        res.json(students)
    })
});




//***********************CLASS MANAGEMENT******************//
///////////////////////manage sections (post, get, put, delete)


router.post('/addClass',function(req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.classes.find({where:{classname:postBody.classname}}).then(function(classes,err){
        if(classes == null){
            model.classes.create({
                classname: postBody.classname,
            }).then(function (res) {
                console.log(res)
            });

            res.statusCode = constants.HTTP.CODES.SUCCESS;
            res.send('class Added Successfully!');
        }
        else{
            res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
            res.send('class already exists!');
        }
    })
    }
);


router.get('/findclassname/:classname', function(req, res) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var classname = req.query.classname;

    model.classes.find({where:{classname:classname}}).then(function(classes){
        if (classes==null)
        {
            res.statusCode = 200;
            return res.send(res.statusCode)
        }
        else
        {
            res.statusCode = 400;
            return res.send(res.statusCode)
        }

    })
});




router.post('/returnclassname', function(req, res){
    postBody = requestHelper.parseBody(req.body);
    model.classes.find({where:{classname:postBody.classname}}).then(function(classes){

        if(classes!=null){
            res.statusCode = 200;
            return res.json(res.statusCode)
        }
        else
        {
            res.statusCode = 400;
            return res.json(res.statusCode);
        }

    })
});



router.get('/viewAllClasses', function(req, res){

    model.classes.findAll().then(function(classes){
        return res.json({classes})

    })
});


router.get('/viewAllClassesOfSection', function(req, res){

    model.classOfsection.findAll().then(function(classOfsections){
        return res.json({classOfsections})

    })
});


router.post('/createClass',function(req, res){

    postBody = requestHelper.parseBody(req.body);

    model.classes.find({
        classname:postBody.classname,
    }).then(function(Class){
        model.section.find({where:{sectionname:postBody.sectionname}}).then(function(Section){
            Class.addSection(Section)
        }).then(function(Class){
            res.json('class added!')
        })
    })
});


router.post('/assignClass',function(req, res){

    postBody = requestHelper.parseBody(req.body);

    model.classes.find({where: {classname:postBody.classname}}).then(function(Class){
        model.section.find({where:{sectionname:postBody.sectionname}}).then(function(Section){
            Class.addSection(Section)
        }).then(function(classsections){
            res.json(classsections)
        })
    })
});


router.get('/sectionOfClass', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var classId = req.query.classId;

    model.classOfsection.find({where:{classId:classId}}).then(function(classOfsections){
            if(classOfsections != null){
                res.json(classOfsections)
            }
            else {
                res.send('record not found!')
            }
        })
    }
)





router.post('/checkclasswithsection', function(req, res){
    postBody = requestHelper.parseBody(req.body);

    model.classOfsection.find({where:{classId:postBody.classId, sectionId: postBody.sectionId}}).then(function(classOfsections){
        if(classOfsections==null){
            res.statusCode = 200;
            return res.json({coursenomatch:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({coursenomatch:true})
        }

    })
})


router.post('/findclassname', function(req, res){
    postBody = requestHelper.parseBody(req.body);


    model.classes.find({where:{classname:postBody.classname}}).then(function(classes){
        if(classes==null){
            res.statusCode = 200;
            return res.json({classnameinvalid:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({classnameinvalid:true})
        }

    })
});


router.post('/addSection',function(req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.section.find({where:{sectionname:postBody.sectionname}}).then(function(sections){
        if(sections == null ){
            if(postBody.sectionname.match(/^[A]+$/) || postBody.sectionname.match(/^[B]+$/) || postBody.sectionname.match(/^[C]+$/) || postBody.sectionname.match(/^[D]+$/)) {
                model.section.create({
                    sectionname: postBody.sectionname
                }).then(function (res) {
                    console.log(res)
                });
                res.statusCode = constants.HTTP.CODES.SUCCESS;
                res.send("Section Added Successfully!");
            }

            else
            {
                res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
                res.send("All Possible Sections Are Added!")
            }
        }
        else
        {
            res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
            res.send(responseHelper.formatResponse(
                constants.MESSAGES.GENERAL.FIELDS_REQUIRED))
        }

    })
    });




router.get('/findSectionByID', function(req, res, next) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.section.find({where:{id:id}}).then(function(sections){
        res.json(sections);
    });
});

router.get('/findSectionByName', function(req, res, next) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var sectionname = req.query.sectionname;

    model.section.find({where:{sectionname:sectionname}}).then(function(sections){
        res.json(sections);
    });
});


router.get('/ViewAllSections',function (req,res) {
    model.section.findAll().then(function (sections) {
        res.json(sections);
    })
});

router.delete('/DeleteSectionBySectionId', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.query.id;

    model.section.destroy({where: {id:id}}).then(function (sections) {
        res.json(sections)
    })
});

router.delete('/DeleteSectionBySectionName', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var sectionname = req.query.sectionname;

    model.section.destroy({where: {sectionname:sectionname}}).then(function (sections) {
        res.json(sections)
    })
});



router.put('/UpdateSection',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var sectionname = req.query.sectionname;

    model.section.find({where: {sectionname:sectionname}}).then(function (sections) {
        return sections.updateAttributes({sectionname:req.body.sectionname});
    }).then(function (sections) {
        res.json(sections);
    })
});


router.get('/FindStudentBySection',function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var secId = req.query.secId;


    model.section.findAll({
        where:{id:secId},
        include:[model.student]
    }).then(function(students){
        res.json(students)
    })
});


//****************COURSES MANAGEMENT***********//




//
// router.post('/addCourse', function(req, res){
//
//     postBody = requestHelper.parseBody(req.body);
//
//     model.course.find({where:{courseno: postBody.courseno}}).then(function(courses){
//         res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
//         res.json(courses);
//         // res.send(responseHelper.formatResponse(
//         //     constants.MESSAGES.GENERAL.SECTION_EXIST))
//     }).then(function(error))
//     {
//
//
//         model.course.create({
//             courseno: postBody.courseno,
//             coursename: postBody.coursename,
//             credithours: postBody.credithours
//
//         }).then(function (res) {
//             res.send('course Added Succcessfully!')
//
//         })
//
//     } })
//


router.post('/findcourseno', function(req, res){
    postBody = requestHelper.parseBody(req.body);


    model.course.find({where:{courseno:postBody.courseno}}).then(function(courses){
        if(courses==null){
            res.statusCode = 200;
            return res.json({coursenomatch:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({coursenomatch:true})
        }

    })
})


router.post('/findcoursename', function(req, res){
    postBody = requestHelper.parseBody(req.body);


    model.course.find({where:{coursename:postBody.coursename}}).then(function(courses){
        if(courses==null){
            res.statusCode = 200;
            return res.json({coursenamematch:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({coursenamematch:true})
        }

    })
});


router.post('/coursenameexist', function(req, res){
    postBody = requestHelper.parseBody(req.body);


    model.course.find({where:{coursename:postBody.coursename}}).then(function(courses){
        if(courses!=null){
            res.statusCode = 200;
            return res.json({coursenamematch:false})
        }
        else
        {
            res.statusCode = 400;
            return res.json({coursenamematch:true})
        }

    })
});



router.get('/viewAllCourses', function(req, res){

    model.course.findAll().then(function(courses){
            return res.json(courses)

    })
});




router.post('/addCourses',function(req, res){

    postBody = requestHelper.parseBody(req.body);

            model.course.create({
                courseno : postBody.courseno,
                coursename: postBody.coursename,
                credithours: postBody.credithours
            }).then(function(courses){
                res.statusCode = 200;
                 res.send(courses)
            })
        })



router.delete('/DeleteCourseByCourseNo/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.course.destroy({where: {id:id}}).then(function (courses) {
        res.json(courses)
    })
});

router.delete('/DeleteCourseByCourseName', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var coursename = req.query.coursename;


    model.course.destroy({where: {coursename:coursename}}).then(function (courses) {
        res.json(courses)
    })
});

router.put('/UpdateCourse/:id',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    model.course.find({where: {id:id}}).then(function (courses) {
        return courses.updateAttributes({courseno:req.body.courseno, coursename:req.body.coursename,credithours:req.body.credithours});
    }).then(function (courses) {
        res.json(courses);
    })
});


router.put('/UpdateStaff/:id',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    model.staff.find({where: {id:id}}).then(function (courses) {
        return staffs.updateAttributes({username:req.body.username,firstname:req.body.firstname,lastname:req.body.lastname,
            gender: req.body.gender,interest:req.body.interest,phone:req.body.phone,email:req.body.email,password:req.body.password,isAdmin:req.body.isAdmin});
    }).then(function (staffs) {
        res.json(staffs);
    })
});





router.put('/UpdateTeacher/:id',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    model.teacher.find({where: {id:id}}).then(function (teachers) {
        return teachers.updateAttributes({username:req.body.username,firstname:req.body.firstname,lastname:req.body.lastname,
            gender: req.body.gender,phone:req.body.phone,email:req.body.email,password:req.body.password,isInvigilator:req.body.isInvigilator});
    }).then(function (teachers) {
        res.json(teachers);
    })
});

router.put('/UpdateStudent/:id',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    model.student.find({where: {id:id}}).then(function (students) {
        return staffs.updateAttributes({username:req.body.username,firstname:req.body.firstname,lastname:req.body.lastname,
            gender: req.body.gender,classId:req.body.classId,sectionname:req.body.sectionname,phone:req.body.phone,email:req.body.email,
            parentmail:req.body.parentemail,password:req.body.password});
    }).then(function (studnets) {
        res.json(students);
    })
});




router.post('/courses',function(req, res){
    postBody = requestHelper.parseBody(req.body);
    var coursename = postBody.coursename;
    var classname = postBody.classname;

    model.course.find({where:{coursename:coursename}}).then(function(Course, err){
        model.classes.find({where:{classname:classname}}).then(function(Class, err){
            if(Course == null){
                res.statusCode = 404;
                return res.json({classmatch:true})
            }

            if(Class == null){
                res.statusCode = 404;
                return res.json({classname: true})
            }
            Course.addClass(Class).then(function (Class) {
                return res.json({classname: false});
            })
        })
    })

});


router.get('/getrecordofcourse/:id', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.course.find({where:{id:id}}).then(function (courses) {
        res.json(courses)
    })
});

router.get('/getrecordofstaff/:id', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.staff.find({where:{id:id}}).then(function (staffs) {
        res.json(staffs)
    })
});

router.get('/getrecordofteacher/:id', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.teacher.find({where:{id:id}}).then(function (teachers) {
        res.json(teachers)
    })
});

router.get('/getrecordofstudent/:id', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.fee.find({where:{id:id}}).then(function (fees) {
        res.json(fees)
    })
});




router.post('/Validinstructorname', function (req , res) {
    postBody =requestHelper.parseBody(req.body);

    model.teacher.find({where:{username:postBody.username}}).then(function(teachers) {
        if(teachers==null)
        {
            res.statusCode =200;
            return res.json({username:true})
        }
        else
        {

            res.statusCode=400
            return res.json({username:false})
        }

    })


})
router.post('/Validstaffname', function (req , res) {
    postBody =requestHelper.parseBody(req.body);

    model.staff.find({where:{username:postBody.username}}).then(function(staffs) {
        if(staffs==null)
        {
            res.statusCode =200;
            return res.json({username:true})
        }
        else
        {

            res.statusCode=400
            return res.json({username:false})
        }

    })


})

router.post('/Validstudentname', function (req , res) {
    postBody =requestHelper.parseBody(req.body);

    model.student.find({where:{username:postBody.username}}).then(function(students) {
        if(students==null)
        {
            res.statusCode =200;
            return res.json({username:true})
        }
        else
        {

            res.statusCode=400
            return res.json({username:false})
        }

    })


})



router.post('/addTeacher',function (req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.teacher.find({where:{username:postBody.username,firstname:postBody.firstname,
        lastname:postBody.lastname,gender:postBody.gender,phone:postBody.phone,
        email:postBody.email, password:postBody.password,isInvigilator:postBody.isInvigilator}}).then(function(teachers){

        if(teachers == null) {
            model.teacher.create({
                username : postBody.username,
                firstname: postBody.firstname,
                lastname: postBody.lastname,
                gender: postBody.gender,
                phone: postBody.phone,
                email: postBody.email,
                password: postBody.password,
                isInvigilator:postBody.isInvigilator,
                expire:postBody.expire
            })

            res.statusCode = 200;
            return res.json(teachers)
        }

        else {

            res.statusCode = 400;
            return res.json(teachers)
        }})})

router.post('/addStaff',function (req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.staff.find({where:{username:postBody.username, firstname:postBody.firstname,
        lastname:postBody.lastname,gender:postBody.gender, interest:postBody.interest,phone:postBody.phone,
        email:postBody.email, password:postBody.password,isAdmin:postBody.isAdmin}}).then(function(staffs){

        if(staffs == null) {
            model.staff.create({
                username : postBody.username,
                firstname: postBody.firstname,
                lastname: postBody.lastname,
                gender: postBody.gender,
                interest: postBody.interest,
                phone: postBody.phone,
                email: postBody.email,
                password: postBody.password,
                isAdmin:postBody.isAdmin

            })

            res.send("Staff Added Successfully!");
        }

        else {
            res.statusCode = 400;
            res.send("staff already exists");
        }})})


router.post('/addStudent',function (req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.student.find({where:{username:postBody.username, firstname:postBody.firstname,
        lastname:postBody.lastname,gender:postBody.gender, classId:postBody.classId, sectionname:postBody.sectionname, phone:postBody.phone,
        email:postBody.email,parentemail:postBody.parentemail, password:postBody.password}}).then(function(students){

        if(students == null) {
            model.student.create({
                username :postBody.username,
                firstname:postBody.firstname,
                lastname:postBody.lastname,
                gender:postBody.gender,
                classId:postBody.classId,
                sectionname:postBody.sectionname,
                phone: postBody.phone,
                email: postBody.email,
                parentemail:postBody.email,
                password: postBody.password,
            })

            res.send("Student Added Successfully!");
        }

        else {
            res.statusCode = 400;
            res.send("student already exists");
        }
})})
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////PHASE 2
/* Blog Creation. */
router.get('/teacher', function(req, res, next) {
    model.teacher.create({
        username:"usama",
        firstname:"usama",
        lastname:"ayaz",
        gender:"Male",
        phone:"03352341467",
        email:"ua@gmail.com",
        password:"12345678",
        isInvigilator:"false"
    }).then(function(teachers){

        model.course.find({coursename:"iad"}).then(function(courses){
            teachers.addCourses(courses).then(function(t){
                res.send("teacher Created");
            });
        });
    });
});//sahi chal rhi hai


router.post('/teacherClass', function(req, res, next) {
    postBody = requestHelper.parseBody(req.body);

    model.teacher.find({where:{firstname:postBody.firstname}}).then(function(teachers){
        if(teachers == null){
            res.statusCode = 400;
            res.send('invalid teacher name!');
        }
        else{
            model.classes.find({where:{classname:postBody.classname}}).then(function(classes) {
                if (classes == null) {
                    res.statusCode = 400;
                    res.send('invalid class name');
                }
                else {
                    model.section.find({where: {sectionname: postBody.sectionname}}).then(function(sections)
                    {
                        if (sections == null) {
                            res.statusCode = 400;
                            res.send('invalid section name');
                        }
                        else {
                            model.course.find({where:{coursename:postBody.subjectname}}).then(function(courses){
                                if(courses == null){
                                    res.statusCode = 400;
                                    res.send('invalid course name');
                                }
                                else{
                                    model.teacherOfclassOfsectionOfsubject.find({where:{firstname:postBody.firstname,classname:postBody.classname,sectionname:postBody.sectionname, subjectname:postBody.subjectname}}).then(function(teacherOfclassOfsectionsOfsubject){
                                        if(teacherOfclassOfsectionsOfsubject == null){

                                            model.teacherOfclassOfsectionOfsubject.create({
                                                firstname: postBody.firstname,
                                                classname: postBody.classname,
                                                sectionname: postBody.sectionname,
                                                subjectname: postBody.subjectname,
                                            }).then(function (teacherOfclassOfsectionsOfsubject) {
                                                res.json(teacherOfclassOfsectionsOfsubject);
                                            })
                                        }
                                        else{
                                            res.statusCode = 400;
                                            return res.statusCode;
                                        }
                                    })

                                }

                            })
                        }
                    })
                }
            })
                }
        })})
//ek dum theek






router.get('/AssignTeacherClassAndSection',function(req, res){
     model.teacher.find({
        firstname:"sumaiya",
    }).then(function(teachers){
        model.classes.find({where:{classname:1}}).then(function(teachers){
            // teachers.addClassOfsections(classOfsections).then(function(classOfsections){
                res.send(teachers);
            // });
        });
    });
});//sahi chal rhi hai




router.get('/courses/teacher',function(req,res){
    model.course.find({where:{id:6}},{
        include: model.teacher
    }).then(function(course){
        course.getTeachers().then(function(teachers){
            res.json(teachers);
        });
    });
});



router.get('/AllCoursesOfTeacher/:firstname',function(req,res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;

    model.teacher.find({where: {firstname:firstname}},{
        include: model.course
    }).then(function(teacher){
        teacher.getCourses().then(function(courses){
            res.json(courses);
        });
    });
});//sahi chal rhi hai


router.get('/TeacherOfCoursesOfClass', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;

    model.teacher.find({where: {firstname:firstname}},{
        include: model.course
    }).then(function(teacher){
        teacher.getCourses().then(function(courses){
            res.json(courses);
        }).then(function(classes){
            classes.getCourses().then(function(courses){
                res.json(courses)
            })
        })
    })
  });
//iska nhi pata


router.get('/ClassWithSection/:classname/:sectionname',function(req,res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;

    model.classes.find({where: {classname:classname}},{
        include: model.section
    }).then(function(classes){
                classes.getSections().then(function(sections){
                res.json(sections)
            })
        });
});


router.get('/ClassWithTeacher/:classname/:sectionname',function(req,res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;

    model.classes.find({where: {classname:classname}},{
        include: model.section
    }).then(function(sections){
        model.section.find({where:{sectionname: sectionname}}).then(function(sections){
            sections.getTeachers().then(function(teachers){
                res.json(teachers)
            })
        })
     });
});

router.post('/returnsubjectname',function(req,res){

    postBody = requestHelper.parseBody(req.body);

    model.course.find({where: {coursename:postBody.coursename}},{
        include: model.classes
    }).then(function(classes){
        model.classes.find({where:{classname:postBody.classname}}).then(function(classes){
            classes.getCourses().then(function(courses){
                   return res.json(courses);

                })
            })
        })
    });



router.get('/teacherExist/:firstname',function(req,res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;
    model.teacher.find({where: {firstname:firstname}}).then(function(teachers) {
        if (teachers == null) {
            res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
            res.send('teacher does not exist!')
        }
        else {
            res.statusCode = constants.HTTP.CODES.SUCCESS;
            res.json(teachers);

        }
    })});

router.get('/Duplicate/:firstname/:classname/:sectionname/:subjectname',function(req,res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;
    var subjectname = req.params.subjectname;

    model.teacherOfclassOfsectionOfsubject.find({where:{firstname:firstname,classname:classname,sectionname:sectionname,subjectname:subjectname}}).then(function(teacherOfclassOfsectionsOfsubject) {
        if (teacherOfclassOfsectionsOfsubject == null) {
            res.statusCode = 200;
            return res.json(res.statusCode)
        }
        else {
            res.statusCode = 400;
            return res.json(res.statusCode);

        }
    })});
//////////////////////////////////////////////////////////////////////////////////////////////instruct CA**************

router.get('/InstructorClassAndCourse/:firstname',function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;

    model.teacherOfclassOfsectionOfsubject.findAll({where:{firstname:firstname}}).then(function(teacherOfclassOfsectionOfsubjects){
        res.json(teacherOfclassOfsectionOfsubjects);
    })
})



router.get('/StudentWithClassAndSections/:classId/:sectionname',function(req, res) {
    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var classId = req.params.classId;
    var sectionname = req.params.sectionname;

    model.student.findAll({where:{classId:classId,sectionname:sectionname}}).then(function (students) {
            res.json(students)
        })
})


router.post('/results',function(req, res){
    postBody = requestHelper.parseBody(req.body);

    model.result.find({where:{firstname: postBody.firstname,lastname:postBody.lastname,
        classname:postBody.classname,sectionname: postBody.sectionname,
        subjectname:postBody.subjectname}}).then(function(results){
            if(results == null){
                    model.result.create({
                    firstname: postBody.firstname,
                    lastname:postBody.lastname,
                    classname:postBody.classname,
                    sectionname: postBody.sectionname,
                    subjectname:postBody.subjectname,
                    grade:postBody.grade,
                    marks: postBody.marks,
                    approve:postBody.approve
                }).then(function(results){
                    res.json('result added successfully!')
                })
            }
            else {
                res.statusCode = 400;

                return res.json(res.statusCode);
            }
    })
});



router.get('/getAllResults/:classname/:sectionname/:subjectname', function(req, res){


    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;
    var subjectname = req.params.subjectname;

    model.result.findAll({where:{classname:classname,sectionname:sectionname,subjectname:subjectname}}).then(function(results){
        if(results == null){
            res.statusCode = 400;
            res.json(res.statusCode)
        }
        else {
            res.json(results)
        }
    })
})


router.get('/getStudentResult/:firstname/:lastname/:classname/:sectionname/:subjectname/:marks/:grade', function(req, res){


    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;
    var subjectname = req.params.subjectname;
    var marks = req.params.marks;
    var grade = req.params.grade;

    model.result.find({where:{firstname:firstname,lastname:lastname,classname:classname,sectionname:sectionname,subjectname:subjectname,marks:marks,grade:grade}}).then(function(results){
        if(results == null){
            res.statusCode = 400;
            res.json(res.statusCode)
        }
        else {
            return res.json(results)
        }
    })
})




router.get('/ResultOfClasses',function(req, res){

    model.notification.findAll({where:{to:"Admin",type:"result",expire:false}}).then(function(notifications){
    if(notifications == null){
        res.send('No New Notification')
    }
    else{
        return res.json(notifications);
    }
    })
});


router.delete('/DeleteResult/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;


    model.result.destroy({where:{id:id}}).then(function (results) {
        res.json(results)
    })
});


router.put('/UpdateResult/:firstname/:lastname/:classname/:sectionname/:subjectname',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;
    var subjectname = req.params.subjectname;

    model.result.find({where:{firstname:firstname,lastname:lastname,classname:classname,sectionname:sectionname,subjectname:subjectname}}).then(function (results) {
        return results.updateAttributes({firstname:req.body.firstname,lastname:req.body.lastname,classname:req.body.classname,
            sectionname:req.body.sectionname,subjectname:req.body.subjectname,marks:req.body.marks,grade:req.body.grade,approve:req.body.approve});

    }).then(function (results) {
        res.json(results);
    })

});


router.put('/Approve/:firstname/:lastname/:classname/:sectionname/:subjectname',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;
    var classname = req.params.classname;
    var sectionname = req.params.sectionname;
    var subjectname = req.params.subjectname;

    model.result.find({where:{firstname:firstname,lastname:lastname,classname:classname,sectionname:sectionname,subjectname:subjectname}}).then(function (results) {
        return results.updateAttributes({firstname:req.body.firstname,lastname:req.body.lastname,classname:req.body.classname,
            sectionname:req.body.sectionname,subjectname:req.body.subjectname,marks:req.body.marks,grade:req.body.grade,approve:req.body.approve});
    }).then(function (results) {
        res.json(results);
    })

});



router.get('/validateteacher/:firstname',function(req,res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;
    var firstname = req.params.firstname;

    model.teacher.find({where:{firstname:firstname}}).then(function(teachers){

        if(teachers == null){
             res.statusCode = 400;
             res.send(res.statusCode);
        }
        else {
                res.statusCode = 200;
                res.send(teachers)
        }

    })

})

router.get('/validateCourses/:firstname/:classname/:subjectname', function(req, res){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;
    var subjectname = req.params.subjectname;
    var classname = req.params.classname;

    model.teacherOfclassOfsectionOfsubject.find({where:{firstname:firstname,classname:classname,subjectname:subjectname}}).then(function(teacherOfclassOfsectionOfsubjects){

        if(teacherOfclassOfsectionOfsubjects == null){
            res.statusCode = 400;
            res.send(res.statusCode);
        }
        else {
            res.statusCode = 200;
            res.send(teacherOfclassOfsectionOfsubjects);
        }

    })

})


router.post('/makedatesheet', function(req, res, next) {

    postBody = requestHelper.parseBody(req.body);

    model.datesheet.find({where:{classname:postBody.classname,tname:postBody.tname,subjectname:postBody.subjectname}}).then(function(datesheets){

            if(datesheets == null) {
                model.datesheet.create({
                    dateexam: postBody.dateexam,
                    classname: postBody.classname,
                    tname: postBody.tname,
                    subjectname: postBody.subjectname,
                    time: postBody.time,
                }).then(function(datesheets) {
                    res.send(datesheets)
                })
            }

            else {
                res.statusCode = 400
                res.send(res.statusCode);
            }
    })
})

router.post('/AssignInvigilators', function (req, res) {

    postBody = requestHelper.parseBody(req.body);


                model.datesheet.find({where:{id:postBody.datesheetId}}).then(function(datesheets){
                    model.invigilatorOfexam.find({where:{datesheetId:postBody.datesheetId,teacherId:postBody.teacherId}}).then(function(invigilatorOfexams){
                        if(invigilatorOfexams == null){
                            model.teacher.find({
                                where: {
                                    firstname: postBody.firstname,
                                    lastname: postBody.lastname
                                }
                            }).then(function (teachers) {
                                teachers.addDatesheets(datesheets).then(function (t) {

                                    res.statusCode = 200;
                                    res.send(res.statusCode);
                                });
                            });
                        }
                        else {
                            res.statusCode = 400;
                            res.send(res.statusCode);
                        }
                    })
                })
});


router.get('/AllExams',function(req, res){

    model.datesheet.findAll().then(function(datesheets){
        if(datesheets == null){

            res.statusCode = 400;
            res.send(res.statusCode);
        }

        else{
            res.json(datesheets);
        }
    })


});

router.get('/FullRecordOfTeacher/:username', function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var username = req.params.username;

    model.teacher.find({where:{username:username}}).then(function(teachers){

        res.json(teachers);
    })
})


router.get('/classWiseDateSheet/:classname', function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var classname = req.params.classname;

    model.datesheet.findAll({where:{classname:classname}}).then(function(datesheets){
        if(datesheets != null) {
            res.send(datesheets);
        }
        else {
            res.statusCode = 400;
            res.send(res.statusCode);
        }
    })

});


router.put('/UpdateExam/:dateexam/:time/:tname/:classname/:subjectname',function (req, res) {
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;
    var dateexam = req.params.dateexam;
    var time = req.params.time;
    var classname = req.params.classname;
    var tname = req.params.tname;
    var subjectname = req.params.subjectname;

    model.datesheet.find({where:{dateexam:dateexam,time:time,tname:tname,classname:classname,subjectname:subjectname}}).then(function (datesheets) {
        return datesheets.updateAttributes({dateexam:req.body.dateexam,time:req.body.time,tname:req.body.tname,
            classname:req.body.classname,subjectname:req.body.subjectname});

    }).then(function (datesheets) {
        res.json(datesheets);
    })

});

router.delete('/DeleteExam/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    model.datesheet.destroy({where:{id:id}}).then(function (datesheets) {
        res.json(datesheets)
    })
});


router.post('/addfees',function (req, res) {
    postBody = requestHelper.parseBody(req.body);

    model.fee.find({where:{accountno:postBody.accountno,firstname:postBody.firstname,
        lastname:postBody.lastname,
        amountpaid:postBody.amountpaid,
        duedate:postBody.duedate,
        dateamountrecieved:postBody.dateamountrecieved,
        amountrecieved:postBody.amountrecieved,onTime:postBody.onTime,fine:postBody.fine}}).then(function(fees){

        if(fees == null) {
            model.fee.create({
                accountno:postBody.accountno,
                firstname:postBody.firstname,
                lastname:postBody.lastname,
                amountpaid:postBody.amountpaid,
                duedate:postBody.duedate,
                annualmisc:postBody.annualmisc,
                milladmisc:postBody.milladmisc,
                healthcaremisc:postBody.healthcaremisc,
                dateamountrecieved:postBody.dateamountrecieved,
                amountrecieved:postBody.amountrecieved,
                onTime:postBody.onTime,
                fine: postBody.fine,
            })
            res.send("fees Added Successfully!");
        }

        else {

            res.statusCode = 400;
            return res.json(fees)
        }})});


router.get('/getAccount/:accountno', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var accountno = req.params.accountno;

    model.fee.find({where:{accountno:accountno}}).then(function (fees) {
        if(fees != null){
            res.statusCode = 400;
            res.send(res.statusCode);
        }
        else{
            res.statusCode = constants.HTTP.SUCCESS;
            res.send(res.statusCode);
        }

    })
});

router.get('/getStudentByName/:firstname/:lastname', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;

    model.student.find({where:{firstname:firstname,lastname:lastname}}).then(function (students) {
        if(students == null){
            res.statusCode =404;
            return res.json({firstname:true,lastname:true})
        }
        else{
            res.statusCode = 200;
            return res.json({firstname:false,lastname:false})
            res.json(students)
        }

    })
});



router.post('/StudentNameInFees', function (req , res) {
    postBody =requestHelper.parseBody(req.body);

    model.fee.find({where:{firstname:postBody.firstname, lastname:postBody.lastname}}).then(function(fees) {
        if(fees==null)
        {
            res.statusCode =200;
            return res.json({firstname:true, lastname:true})
        }
        else
        {

            res.statusCode=400
            return res.json({firsname:false, lastname:false})
        }

    })


})



router.get('/getrecordoffees/', function(req, res){

    // console.log("ayaa hn?")
    // var url_parts = url.parse(req.url,true);
    // var query =  url_parts.query;
    //  var accountno = req.params.accountno;


    model.fee.findAll().then(function(fees){
        return res.json(fees)

    })
});

router.delete('/DeleteFees/:id', function(req,res, next){
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.fee.destroy({where: {id:id}}).then(function (fees) {
        res.json(fees)
    })
});

router.get('/getrecordforfees/:id', function(req, res){
    console.log('api mai aya')
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var id = req.params.id;

    console.log(id);
    model.fee.find({where:{id:id}}).then(function (fees) {
        res.json(fees)
    })
});

router.get('/Defaulters', function(req, res){

    model.fee.findAll({where:{onTime:"false"}}).then(function(fees){
        res.send(fees);
    })
});

router.put('/UpdateFees/:accountno/:firstname/:lastname/:amountpaid/:duedate/:annualmisc/:milladmisc/:healthcaremisc/:dateamountrecieved/:amountrecieved/:onTime/:fine',function (req, res) {

        var url_parts = url.parse(req.url,true);
        var query =  url_parts.query;

        var accountno=req.params.accountno;
        var firstname = req.params.firstname;
        var lastname=req.params.lastname;
        var amountpaid=req.params.amountpaid;
        var duedate=req.params.duedate;
        var annualmisc=req.params.annualmisc;
        var milladmisc=req.params.milladmisc;
        var healthcaremisc=req.params.healthcaremisc;
        var dateamountrecieved=req.params.dateamountrecieved;
        var amountrecieved=req.params.amountrecieved;
        var onTime=req.params.onTime;
        var fine= req.params.fine;

    model.fee.find({where:{accountno:accountno,
        firstname:firstname,
        lastname:lastname,
    }}).then(function (fees) {
        return fees.updateAttributes({
            accountno:accountno,
            firstname:firstname,
            lastname:lastname,
            amountpaid:amountpaid,
            duedate:duedate,
            annualmisc:annualmisc,
            milladmisc:milladmisc,
            healthcaremisc:healthcaremisc,
            dateamountrecieved:dateamountrecieved,
            amountrecieved:amountrecieved,
            onTime:onTime,
            fine:fine,
        });

    }).then(function (fees) {
        res.json(fees);
    })

});


router.post('/postannouncements',function(req, res){

    postBody =requestHelper.parseBody(req.body);

     model.announcement.create({
         content:postBody.content,
         sendto:postBody.sendto,
     }).then(function(announcements){
         res.send(announcements)
     })

})

router.get('/returnAnnouncement/:sendto',function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var sendto = req.params.sendto;

    model.announcement.findAll({where: {sendto:sendto}}).then(function(notifications){
        if(notifications == null){
            res.statusCode = 400;
            res.send(res.statusCode);
        }

        else {
            res.statusCode = 200;
            res.json(notifications);
        }
    })
});


router.get('/getnames/:firstname/:lastname', function(req, res){

    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;

    model.fee.findAll({where:{firstname:firstname,lastname:lastname}}).then(function(fees){

        if(fees == null){
            res.statusCode = 400;
            res.send(res.statusCode);
        }
        else{
            res.json(fees);
        }
    })


})



module.exports = router;
