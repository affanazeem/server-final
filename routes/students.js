var express = require('express');
var router = express.Router();
var model = require("../models");
var constants = require("../config/constants");
var requestHelper = require("../helpers/request");
var responseHelper = require("../helpers/response");
var url = require('url');
var jwt = require('jsonwebtoken');


router.get('/loginstudent/:username/:password', function(req, res, next) {


    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var username = req.params.username;
    var password= req.params.password;

    model.student.find({where:{username:username, password:password}}).then(function (students) {

        if(students == null)
        {
            res.statusCode = 404;
            res.send(students);
        }
        else
        {
            res.json(students);
        }
    })
});

router.get('/getAllDetails/:username', function(req, res, next) {


    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var username = req.params.username;

    model.student.find({where:{username:username}}).then(function (students) {

        if(students == null)
        {
            res.statusCode = 404;
            res.send(students);
        }
        else
        {
            res.json(students);
        }
    })
});


router.get('/allResults/:firstname/:lastname', function(req, res){

    var url_parts = url.parse(req.url, true);
    var query = url_parts.query;
    var firstname = req.params.firstname;
    var lastname = req.params.lastname;

    model.result.findAll({where:{firstname:firstname,lastname:lastname,approve:"true"}}).then(function(results){
        res.json(results);}

)

});

module.exports = router;