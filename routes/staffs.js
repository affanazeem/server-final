var express = require('express');
var router = express.Router();
var model = require("../models");
var constants = require("../config/constants");
var requestHelper = require("../helpers/request");
var responseHelper = require("../helpers/response");
var url = require('url');




router.post('/loginstaff', function(req, res, next) {
    postBody = requestHelper.parseBody(req.body);

    model.staff.find({where: {username:postBody.username, password:postBody.password}}).then(function (staffs) {

        if(staffs == null)
        {
            res.statusCode = 404;
            res.send(staffs);
        }
        else
        {
            return res.json(staffs);
        }
    })
});


router.post('/addFees',function (req, res) {
    postBody = requestHelper.parseBody(req.body);
    if(ValidateStudent(postBody,req, res)){
        model.managefee.create({
            studentID: postBody.studentID,
            semesterfees: postBody.semesterfees,
            examfees: postBody.examfees,
            latefees: postBody.latefees,
            totalfees: postBody.semesterfees+postBody.examfees+postBody.latefees,
            duedate: postBody.duedate,
            status: postBody.status
        }).then(function (res) {
        });

        res.send("Fees Record Added Successfully!");

    }

    else
    {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
        // res.send(responseHelper.formatResponse(
        //     constants.MESSAGES.GENERAL.FIELDS_REQUIRED))
    }
});


function ValidateStudent(student,req, res) {

    studentID = student.studentID;
    if (!model.managefee.find({studentID}).then(function (managefees, req, res) {
        })){
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
        res.send(responseHelper.formatResponse(
            constants.MESSAGES.GENERAL.STUDENT_EXIST))
    }

    else {
        if (student.studentID != null && student.semesterfees != null && student.examfees != null && student.latefees != null ) {
            return true;
        }

        return false;
    }
}




router.get('/viewFeesByID',function (req, res) {
    postBody = requestHelper.parseBody(req.body);
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var studentID = req.query.studentID;

    model.managefee.find({where: {studentID:studentID}}).then(function (managefees) {
        res.json(managefees);
    })
});


router.delete('/DeleteFeesByID', function(req,res, next){
    postBody = requestHelper.parseBody(req.body);
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var  studentId = req.query.studentID;

    model.managefee.destroy({where: {id:id}}).then(function (managefees) {
        res.json(managefees)
    })
});

router.put('/UpdateFeesByID',function (req, res) {
    postBody = requestHelper.parseBody(req.body);
    var url_parts = url.parse(req.url,true);
    var query =  url_parts.query;
    var studentID = req.query.studentID;

    model.managefee.find({where: {studentID:studentID}}).then(function (managefees) {
        return managefees.updateAttributes({studentID:req.body.studentID, semesterfees:req.body.semesterfees, examfees:req.body.examfees,  phone:req.body.phone , email:req.body.emil, latefees:req.body.latefees, totalfees:req.body.totalfees, duedate:req.body.duedate,  slug:req.body.slug});
    }).then(function (managefees) {
        res.json(managefees);
    })
});

function StudentNotExist(postBody, res){
    if(model.managefee.findAll({where:{studentID:postBody.studentID}})){
        res.send('student already exist!')
        ;    }
}

module.exports = router;