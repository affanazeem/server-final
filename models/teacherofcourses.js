'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacherOfcourses = sequelize.define('teacherOfcourses', {
    teacherId: DataTypes.INTEGER,
    courseId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });
  return teacherOfcourses;
};