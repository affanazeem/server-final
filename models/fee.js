'use strict';
module.exports = function(sequelize, DataTypes) {
  var fee = sequelize.define('fee', {
    accountno: DataTypes.INTEGER,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    amountpaid: DataTypes.INTEGER,
    duedate: DataTypes.DATE,
    annualmisc: DataTypes.INTEGER,
    milladmisc: DataTypes.INTEGER,
    healthcaremisc: DataTypes.INTEGER,
    dateamountrecieved: DataTypes.DATE,
    amountrecieved: DataTypes.INTEGER,
    onTime: DataTypes.STRING,
    fine: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return fee;
};