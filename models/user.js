'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    dob: DataTypes.DATE,
    gender: DataTypes.STRING,
    roleId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
          User.hasMany(models.Roles, {foreignKey: 'roleId',as: "Roles"})
      }
    }
  });
  return User;
};