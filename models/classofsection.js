'use strict';
module.exports = function(sequelize, DataTypes) {
  var classOfsection = sequelize.define('classOfsection', {
    classId: DataTypes.INTEGER,
    sectionId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
         }
    }
  });
  return classOfsection;
};