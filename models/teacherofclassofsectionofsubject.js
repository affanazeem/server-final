'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacherOfclassOfsectionOfsubject = sequelize.define('teacherOfclassOfsectionOfsubject', {
    firstname: DataTypes.STRING,
    classname: DataTypes.STRING,
    sectionname: DataTypes.STRING,
    subjectname: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  });
  return teacherOfclassOfsectionOfsubject;
};