'use strict';
module.exports = function(sequelize, DataTypes) {
  var student = sequelize.define('student', {
    username: DataTypes.STRING,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    gender: DataTypes.STRING,
    classId: DataTypes.STRING,
    sectionname: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    parentemail: DataTypes.STRING,
    password: DataTypes.STRING,
    expire: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return student;
};