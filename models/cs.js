'use strict';
module.exports = function(sequelize, DataTypes) {
  var cs = sequelize.define('cs', {
    classId: DataTypes.INTEGER,
    sectionId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return cs;
};