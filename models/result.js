'use strict';
module.exports = function(sequelize, DataTypes) {
  var result = sequelize.define('result', {
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    classname: DataTypes.STRING,
    sectionname: DataTypes.STRING,
    subjectname: DataTypes.STRING,
    marks: DataTypes.INTEGER,
    grade: DataTypes.STRING,
    approve: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return result;
};