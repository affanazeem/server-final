'use strict';
module.exports = function(sequelize, DataTypes) {
  var announcement = sequelize.define('announcement', {
    content: DataTypes.STRING,
    sendto: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return announcement;
};