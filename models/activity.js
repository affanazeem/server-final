'use strict';
module.exports = function(sequelize, DataTypes) {
  var activity = sequelize.define('activity', {
    classId: DataTypes.INTEGER,
    sectionId: DataTypes.INTEGER,
    announcement: DataTypes.STRING,
    homework: DataTypes.STRING,
    assignemnt: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return activity;
};