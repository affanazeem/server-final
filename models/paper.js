'use strict';
module.exports = function(sequelize, DataTypes) {
  var paper = sequelize.define('paper', {
    tname: DataTypes.STRING,
    classname: DataTypes.STRING,
    sectionname: DataTypes.STRING,
    subjectname: DataTypes.STRING,
    content: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return paper;
};