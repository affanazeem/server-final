'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacherOfclassOfsection = sequelize.define('teacherOfclassOfsection', {
    firstname: DataTypes.STRING,
    classname: DataTypes.STRING,
    sectionname: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return teacherOfclassOfsection;
};