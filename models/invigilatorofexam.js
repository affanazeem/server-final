'use strict';
module.exports = function(sequelize, DataTypes) {
  var invigilatorOfexam = sequelize.define('invigilatorOfexam', {
    teacherId: DataTypes.INTEGER,
    datesheetId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return invigilatorOfexam;
};