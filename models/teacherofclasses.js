'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacherOfclasses = sequelize.define('teacherOfclasses', {
    teacherId: DataTypes.INTEGER,
    classId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });
  return teacherOfclasses;
};