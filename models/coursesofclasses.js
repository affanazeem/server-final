'use strict';
module.exports = function(sequelize, DataTypes) {
  var coursesOfclasses = sequelize.define('coursesOfclasses', {
    courseId: DataTypes.INTEGER,
    classId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });
  return coursesOfclasses;
};