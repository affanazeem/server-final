'use strict';
module.exports = function(sequelize, DataTypes) {
  var notification = sequelize.define('notification', {
    to: DataTypes.STRING,
    from: DataTypes.STRING,
    classname: DataTypes.STRING,
    sectionname: DataTypes.STRING,
    subjectname: DataTypes.STRING,
    type: DataTypes.STRING,
    expire: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return notification;
};