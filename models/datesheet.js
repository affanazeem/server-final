'use strict';
module.exports = function(sequelize, DataTypes) {
  var datesheet = sequelize.define('datesheet', {
    dateexam: DataTypes.STRING,
    classname: DataTypes.STRING,
    tname: DataTypes.STRING,
    subjectname: DataTypes.STRING,
    time: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
          datesheet.belongsToMany(models.teacher, {
              through: {
                  model: models.invigilatorOfexam,
                  foreignKey: "datesheetId",
                  as: "Teacher"
              }
          });
      }
    }
  });
  return datesheet;
};