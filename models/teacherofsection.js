'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacherOfsection = sequelize.define('teacherOfsection', {
    sectionId: DataTypes.INTEGER,
    teacherId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return teacherOfsection;
};