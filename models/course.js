'use strict';
module.exports = function(sequelize, DataTypes) {
  var course = sequelize.define('course', {
    courseno: DataTypes.STRING,
    coursename: DataTypes.STRING,
    credithours: DataTypes.STRING
  }, {
    classMethods: {
        associate: function (models) {
            course.belongsToMany(models.classes, {
                through: {
                    model: models.coursesOfclasses,
                    foreignKey: "courseId",
                    as: "Class"
                }
            });

                course.belongsToMany(models.teacher, {
                    through: {
                        model: models.teacherOfcourses,
                        foreignKey: "courseId",
                        as: "Teacher"
                    }
                });

        }
    }
    });
  return course;
};