'use strict';
module.exports = function(sequelize, DataTypes) {
  var classes = sequelize.define('classes', {
    classname: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
          classes.belongsToMany(models.course,{
              through:{
                  model:models.coursesOfclasses,
                  foreignKey:"classId",
                  as:"Course"
              }
          });

          classes.belongsToMany(models.section, {
              through: {
                  model: models.classOfsection,
                  foreignKey: "classId",
                  as: "Section"
              }
          });

          classes.belongsToMany(models.teacher, {
              through: {
                  model: models.teacherOfclasses,
                  foreignKey: "classId",
                  as: "Teacher"
              }
          });
        }
    }
  });
  return classes;
};
