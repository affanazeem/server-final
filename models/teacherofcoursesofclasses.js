'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacherOfcoursesOfclasses = sequelize.define('teacherOfcoursesOfclasses', {
    teacherOfcoursesId: DataTypes.INTEGER,
    classId: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return teacherOfcoursesOfclasses;
};