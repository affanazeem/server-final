'use strict';
module.exports = function(sequelize, DataTypes) {
  var login = sequelize.define('login', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    post: DataTypes.STRING,
    expire: DataTypes.BOOLEAN
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return login;
};