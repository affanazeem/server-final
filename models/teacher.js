'use strict';
module.exports = function(sequelize, DataTypes) {
  var teacher = sequelize.define('teacher', {
    username: DataTypes.STRING,
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    gender: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    isInvigilator: DataTypes.BOOLEAN,
    expire: DataTypes.BOOLEAN
  }, {
      classMethods: {
          associate: function(models) {

              teacher.belongsToMany(models.course, {
                  through: {
                      model: models.teacherOfcourses,
                      foreignKey: "teacherId",
                      as: "Course"
                  }
              });

              teacher.belongsToMany(models.datesheet, {
                  through: {
                      model: models.invigilatorOfexam,
                      foreignKey: "teacherId",
                      as: "Datesheet"
                  }
              });

              teacher.belongsToMany(models.classes, {
                  through: {
                      model: models.teacherOfclasses,
                      foreignKey: "teacherId",
                      as: "Class"
                  }
              });

              teacher.belongsToMany(models.section, {
                  through: {
                      model: models.teacherOfsection,
                      foreignKey: "teacherId",
                      as: "Section"
                  }
              });


          },

      }
  });
    return teacher;
};