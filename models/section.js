
'use strict';
module.exports = function(sequelize, DataTypes) {
  var section = sequelize.define('section', {
    sectionname: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
          section.hasMany(models.student, {foreignKey:"sectionname"});

          section.belongsToMany(models.classes, {
              through: {
                  model: models.classOfsection,
                  foreignKey: "sectionId",
                  as: "Class"
              }
          });

          section.belongsToMany(models.teacher, {
              through: {
                  model: models.teacherOfsection,
                  foreignKey: "sectionId",
                  as: "Teacher"
              }
          });
      }
    }
  });
  return section;
};