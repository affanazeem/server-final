'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('fees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      accountno: {
        type: Sequelize.INTEGER
      },
      firstname: {
        type: Sequelize.STRING
      },
      lastname: {
        type: Sequelize.STRING
      },
      amountpaid: {
        type: Sequelize.INTEGER
      },
      duedate: {
        type: Sequelize.DATE
      },
      annualmisc: {
        type: Sequelize.INTEGER
      },
      milladmisc: {
        type: Sequelize.INTEGER
      },
      healthcaremisc: {
        type: Sequelize.INTEGER
      },
      dateamountrecieved: {
        type: Sequelize.DATE
      },
      amountrecieved: {
        type: Sequelize.INTEGER
      },
      onTime: {
        type: Sequelize.STRING
      },
      fine: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('fees');
  }
};