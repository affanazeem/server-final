'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('results', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstname: {
        type: Sequelize.STRING
      },
      lastname: {
        type: Sequelize.STRING
      },
      classname: {
        type: Sequelize.STRING
      },
      sectionname: {
        type: Sequelize.STRING
      },
      subjectname: {
        type: Sequelize.STRING
      },
      marks: {
        type: Sequelize.INTEGER
      },
      grade: {
        type: Sequelize.STRING
      },
      approve: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('results');
  }
};