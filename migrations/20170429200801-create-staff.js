'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('staffs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING,
          allowNull: false
      },
      firstname: {
        type: Sequelize.STRING,
          allowNull: false
      },
      lastname: {
        type: Sequelize.STRING,
          allowNull: false
      },
      gender: {
        type: Sequelize.STRING,
          allowNull: false
      },
      interest: {
        type: Sequelize.STRING,
          allowNull: false
      },
      phone: {
        type: Sequelize.STRING,
          allowNull: false
      },
      email: {
        type: Sequelize.STRING,
          allowNull: false
      },
      password: {
        type: Sequelize.STRING,
          allowNull: false
      },
      isAdmin: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('staffs');
  }
};