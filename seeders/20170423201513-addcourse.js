'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {

      return queryInterface.bulkInsert('courses', [{
          courseno: '100',
          coursename: 'english',
          credithours:'30',
      },
          {
              courseno: '101',
              coursename: 'urdy',
              credithours:'30',
          }, {
              courseno: '102',
              coursename: 'maths',
              credithours: '30',
          },
          {
              courseno: '103',
              coursename: 'islamiat',
              credithours: '30',
          },
          {
              courseno: '104',
              coursename: 'pak studies',
              credithours: '30',
          }]);

  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
