'use strict';

module.exports = {
  up: function (queryInterface, Sequelize){

      return queryInterface.bulkInsert('Roles', [{
          name: 'Admin',
          slug: 'ADMINISTRATOR',
      },
          {
              name: 'Staff',
              slug: 'STAFF',
          },
          {
              name: 'Teacher',
              slug: 'TEACHER',
          },
          {
              name: 'Student',
              slug: 'STUDENT',
          }
      ]);

  },


    down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
