'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.bulkInsert('teachers', [{
          username:"Farhan",
          firstname:"Farhan",
          lastname:"Ahmed",
          gender:"Male",
          phone:"0332369863",
          email:"farhan123@hotmail.com",
          password:"123456",
          isInvigilator:false,
          expire: false

      }])
  },


  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};
