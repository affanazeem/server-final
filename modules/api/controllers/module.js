var express = require('express');
var router = express.Router();
var  authenticatecontroller = require('./controllers/authenticate-controller');

router.post('/authenticate', authenticatecontroller.authenticate);


module.exports = router;



