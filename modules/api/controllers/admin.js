var constants = require("../../../config/constants");
var requestHelper = require("../../../helpers/request");
var responseHelper = require("../../../helpers/response");



var admin = {
    title: "Hello World",
    statusCode: constants.HTTP.CODES.SUCCESS
}

admin.user = function (req, res,next) {

    postBody = requestHelper.parseBody(req.body);

    let admin = {
        username: postBody.username,
        password: postBody.password,
        roleId:postBody.roleId
    };
    if (validationAdmin(admin)) {
        res.statusCode = constants.HTTP.CODES.SUCCESS;
        res.send(responseHelper.formatResponse(
            constants.MESSAGES.CALC.SUCCESS,
            {"username: ": admin.username, "pass":admin.password,"roleId":admin.roleId}
        ))
    }
    else {
        res.statusCode = constants.HTTP.CODES.BAD_REQUEST;
        res.send(responseHelper.formatResponse(
            constants.MESSAGES.GENERAL.FIELDS_REQUIRED
        ))
    }
};

function validationAdmin(admin) {

    if (admin.username != null && admin.username.match(/^[A-Z a-z]+$/) && admin.password != null && admin.password.length >= 7) {
        return true;
    }
    return false;
}



module.exports = admin;
