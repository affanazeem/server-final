var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var mysql = require('mysql');
var cors = require('cors');

var index = require('./routes/index');
var admin_routes = require('./routes/admins');
var staff_routes = require('./routes/staffs');
var teacher_routes = require('./routes/teachers');
var student_routes = require('./routes/students');

var secureRoutes = express.Router();


var authenticateController  = require('./modules/api/controllers/authenticateController');
var app = express();
//
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(cors());

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname,'/public')));
app.use('/', index);
app.use('/admins', admin_routes);
app.use('/staffs', staff_routes);
app.use('/teachers', teacher_routes);
app.use('/students', student_routes);

app.get('/name', function (req, res, next) {
    res.json({msg: 'This is CORS-enabled for all origins!'})
})




// app.use('/secureapi',secureRoutes);
// process.env.SECRET_KEY = "admin";
// secureRoutes.use(function (req, res, next) {
//     var token = req.body.token || req.headers['token'];
//     if(token){
//       jwt.verify(token, process.env.SECRET_KEY, function (err, decode) {
//           if(err){
//           res.statusCode(500).send("Invalid Token");
//           }
//           else
//           {
//             res.send('yeahan!');
//             app.use('/admin',admin.user);
//           }
//       })
//       res.send('token :D');
//     }
//     else
//     {
//       res.send('token :(');
//     }
// })
//
// secureRoutes.post('/admin',admin.user);

app.get('/api/authenticate',authenticateController.authenticate);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


var con = mysql.createConnection({
  host:"localhost",
    user:"root",
    password:"",
    database:"database_development"
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
